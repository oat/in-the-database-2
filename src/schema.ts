/* eslint-disable no-unused-vars */
import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;
 
export enum SMVersion {
	OPENITG,
	FUCKEXE,
	NOTITG_V1,
	NOTITG_V2,
	NOTITG_V3,
	NOTITG_V3_1,
	NOTITG_V4,
	NOTITG_V4_0_1,
	STEPMANIA_3_95,
	STEPMANIA_5_0,
	STEPMANIA_5_1,
	STEPMANIA_5_2,
	STEPMANIA_5_3,
}

const Sample = new Schema({
	start: {type: Number, default: 0},
	length: {type: Number, default: 0}
});

const UserRating = new Schema({
	rating: {type: Number, default: 0},
	createdAt: Date,
	user: {type: String, default: '00000000-0000-4000-a000-000000000000'}
});

const Chart = new Schema({
	type: {type: String, default: 'dance-single'},
	name: {type: String, default: ''},
	difficulty: {type: String, default: 'Challenge'},
	radarvalues: [Number],

	rating: {type: Number, default: 0},
	ratingsVote: {type: [UserRating], default: []},

	spoilered: {type: Boolean, default: false},
	hidden: {type: Boolean, default: false},

	steps: {type: Number, default: 0},
	mines: {type: Number, default: 0},
	jumps: {type: Number, default: 0},
	hands: {type: Number, default: 0},
	holds: {type: Number, default: 0},
	rolls: {type: Number, default: 0},
});

const Comment = new Schema({
	author: {type: String, default: '00000000-0000-4000-a000-000000000000'},
	createdAt: Date,
	content: {type: String, default: ''}
});

const FileSchema = new Schema({
	id: {type: Number, default: 0},

	title: {type: String, default: 'unknown'},
	titleTranslit: String,
	artist: {type: String, default: 'unknown'},
	artistTranslit: String,
	subtitle: String,
	subtitleTranslit: String,

	credit: String,
	uploader: {type: String, default: '00000000-0000-4000-a000-000000000000'},

	sample: Sample,
	bpms: {type: Object, default: {'0': 0}},

	charts: {type: [Chart], default: []},

	description: {type: String, default: ''},
	createdAt: Date,
	smVersion: {type: Number, default: 0}, // see SMVersion enum
	ytLink: String,
	customLink: String,
	hidden: {type: Boolean, default: false},

	comments: {type: [Comment], default: []},
});

export const File = mongoose.model('File', FileSchema);

const UserSchema = new Schema({ // this is pretty much just a discord user lol
	id: {type: String, default: 'notgiven!!!!!!!!!!!!'}, // discord id, cus longass number
	createdAt: Date,

	// caching
	username: {type: String, default: 'User'},
	discriminator: {type: String, default: '0000'},
	avatar: String,

	// used internally
	uuid: {type: String, default: '00000000-0000-4000-a000-000000000000'},

	approvedUpload: {type: Boolean, default: false},
	approvedRate: {type: Boolean, default: false},
	approvedComment: {type: Boolean, default: false},
});

export const User = mongoose.model('User', UserSchema);

const PackSchema = new Schema({
	author: {type: String, default: '00000000-0000-4000-a000-000000000000'},
	files: {type: [Number], default: []}, // ids
	name: {type: String, default: 'Pack'},
	description: {type: String, default: ''},
	createdAt: Date,

	hidden: {type: Boolean, default: false},
});

export const Pack = mongoose.model('Pack', PackSchema);