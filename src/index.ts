import * as express from 'express';
import * as mongoose from 'mongoose';
import * as fs from 'fs';
import * as winston from 'winston';
import * as fileUpload from 'express-fileupload';
import * as session from 'express-session';
import * as favicon from 'serve-favicon';
const MongoStore = require('connect-mongo')(session);

import * as format from './lib/format';
import { File, User } from './schema';

import * as upload from './upload';
import * as auth from './auth';

// .env stuff
require('dotenv').config();

const config = JSON.parse(fs.readFileSync('./config/config.json', {encoding: 'utf8'}));

const db = mongoose.connect(`${config.dbconnectionURL}/${config.dbname}`, {
	useNewUrlParser: true, // idfk what any of this does i just copied an example
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true
});

const logger = winston.createLogger({
	level: 'info',
	format: winston.format.combine(
		winston.format.timestamp(),
		winston.format.printf(log => `${format.formatTime(new Date(log.timestamp))} | ${log.message}`)
	),
	transports: [
		new winston.transports.File({filename: `${config.name}-error.log`, level: 'error'}),
		new winston.transports.File({filename: `${config.name}.log`}),
		new winston.transports.Console({
			format: winston.format.combine(
				winston.format.colorize(),
				winston.format.timestamp(),
				winston.format.printf(log => 
					`${format.formatTime(new Date(log.timestamp))} - [${log.level}] ${log.message}`
				)
			),
			level: process.env.DEBUG === 'true' ? 'silly' : 'info'
		})
	]
});

logger.info('connecting to mongodb database');
db.then(() => {
	logger.info('connected to database!');

	const app = express();

	// @ts-ignore
	app.use(express.urlencoded({extended: true}));
	app.use(favicon('assets/icon.ico'));
	app.use(fileUpload({limits: { fileSize: 50 * 1024 * 1024 }}));
	app.use(express.static('public', {extensions:  ['html', 'htm']}));
	app.use(express.static('storage', {extensions:  ['zip']}));
	app.use(session({
		name: 'funnyuserdata',
		secret: 'wenis',
		store: new MongoStore({ mongooseConnection: mongoose.connection }),
		cookie: {
			maxAge: 1000 * 60 * 60 * 24 * 365 * 10, // 10 years
			httpOnly: true,
			sameSite: 'lax',
		},
		resave: false,
		saveUninitialized: true
	}));
	app.use('/assets', express.static('assets'));

	app.set('db', db);
	app.set('config', config);
	app.set('logger', logger);

	upload.run(app);
	auth.run(app);

	app.get('/api/list', async (req, res) => {
		const files = await File.find({});

		const docs = [];
		for (const doc of files) {
			const d: any = doc.toJSON();

			d.editable = false;
			if (req.session) d.editable = req.session.uuid === d.uploader;

			const user = await User.find({uuid: d.uploader});
			if (user) {
				d.uploaderJSON = user[0].toJSON(); // this is built upon 20 layers of metajank and i despise it
				docs.push(d);
			}
		}

		// TODO: filter out _id and __v? possibly more
		res.send(docs);
	});

	app.get('*', (req, res) => {
		res.status(404).send('404');
	});

	app.listen(config.port, () => {
		logger.info(`expressjs server launched on port ${config.port}, should now function properly`);
	});
});