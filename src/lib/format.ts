export function formatTime(date: Date) : string {
	const hours = date.getUTCHours();
	const minutes = date.getUTCMinutes();

	return `${hours < 10 ? '0' : ''}${hours}:${minutes < 10 ? '0' : ''}${minutes} UTC`;
}