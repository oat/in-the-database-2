export function parseSM(data: string) {
	// steps
	const difficulties = [];
	const steps = data.split('#NOTES:');
	steps.slice(1);

	for (const step of steps) {
		if (step.includes(';')) {
			const diff: any = {};
			const stepsSplit = step.split(';')[0].split(':').map(seg => seg.trim());

			if (stepsSplit.length === 6) {
				diff.type = stepsSplit[0];
				diff.name = stepsSplit[1];
				diff.difficulty = stepsSplit[2];
				diff.rating = Number(stepsSplit[3]);
				diff.radarvalues = stepsSplit[4].split(',').map(v => Number(v));

				const chart = stepsSplit[5];
				diff.rawChart = chart;

				diff.steps = chart.split(/[124]/g).length - 1;
				diff.mines = chart.split('M').length - 1;
				diff.jumps = chart.split(/[124]0{0,2}[124]/g).length - 1;
				diff.hands = chart.split(/[124]0{0,1}[124]0{0,1}[124]/g).length - 1;
				diff.holds = chart.split('2').length - 1;
				diff.rolls = chart.split('4').length - 1;

				diff.steps -= diff.jumps; // jumps are counted as 1 step

				difficulties.push(diff);
			}
		}
	}

	data = data.replace(/[\n\r]/g,'');

	// metadata
	const lines = data.split(';').filter(l => l.startsWith('#'));
	const obj: any = {};

	for (const l of lines) {
		const key = l.split(':')[0].slice(1);
		let value: any = l.split(':')[1];

		if (value !== '' && !(key === 'FGCHANGES' || key === 'BGCHANGES' || key === 'BETTERBGCHANGES' || key === 'SPELLCARDS')) { // no
			if (!isNaN(value)) value = Number(value);
			if (value === 'YES') value = true;
			if (value === 'NO') value = false;
			if (typeof value === 'string' && value.includes('=')) { // likely a map
				const keys = value.split(',').map(v => v.split('=')[0]);
				const values = value.split(',').map(v => v.split('=')[1]);
				const map = {};

				for (const i in keys) {
					map[String(Number(keys[i])).replace('.', ',')] = Number(values[i]); // afaik maps are only numbers?
				}

				value = map;
			}
	
			obj[key.toLowerCase()] = value;
		}
	}

	obj.charts = difficulties;

	return obj;
}