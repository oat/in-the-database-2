import { User } from './schema';
import * as uuid from 'uuid';

const API_ENDPOINT = 'https://discord.com/api/v6';

const axios = require('axios').default;

export function run(app) {
	app.get('/discordauth', async (req, res) => {
		const code = req.query.code;
		const url = `http://${req.headers.host}/discordauth`;

		if (code) {
			try {
				const data = new URLSearchParams({
					client_id: process.env.DISCORD_OAUTH_CLIENTID,
					client_secret: process.env.DISCORD_OAUTH_CLIENTSECRET,
					grant_type: 'authorization_code',
					code: code,
					redirect_uri: url,
					scope: 'identify'
				});
	
				const postRes = await axios.post(`${API_ENDPOINT}/oauth2/token`, data, {
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					}
				});

				const userInfo = await axios.get(`${API_ENDPOINT}/users/@me`, {
					headers: {
						authorization: `${postRes.data.token_type} ${postRes.data.access_token}`
					}
				});

				const users = await User.find({id: String(userInfo.data.id)});
				let userUuid = '';

				if (users.length === 0) {
					let newUuid = uuid.v4();

					while (User.find({uuid: newUuid})[0]) {
						newUuid = uuid.v4();
					}

					const newUser = new User({
						id: String(userInfo.data.id),
						createdAt: new Date(),

						username: userInfo.data.username,
						discriminator: userInfo.data.discriminator,
						avatar: userInfo.data.avatar,

						uuid: newUuid,
					});

					userUuid = newUser.get('uuid');
					newUser.save();
				} else {
					const user = users[0];
					userUuid = user.get('uuid');

					user.set('id', String(userInfo.data.id));
					user.set('username', userInfo.data.username);
					user.set('discriminator', userInfo.data.discriminator);
					user.set('avatar', userInfo.data.avatar);
				}

				req.session!.discord = userInfo.data;
				req.session!.uuid = userUuid;
				res.send(`logged in as ${userInfo.data.username}#${userInfo.data.discriminator}<br><img src="https://media.discordapp.net/avatars/${userInfo.data.id}/${userInfo.data.avatar}.png"><br>ur useruuid is ${userUuid}`);
			} catch(err) {
				res.send(`whoooops<br>${err}`);
				console.error(err);
			}
		} else {
			res.send(`<a href="https://discord.com/api/oauth2/authorize?client_id=${process.env.DISCORD_OAUTH_CLIENTID}&redirect_uri=${encodeURI(url)}&response_type=code&scope=identify">Click here!!</a>`);
		}
	});
}